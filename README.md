# UI5 application using custom middlewares and tasks

This sample application shows the consumption of the custom middlewares and the custom tasks.

Peek at both [`ui5.yaml`](ui5.yaml) and [`package.json`](package.json) in order to learn about the `ui5-tooling` configuration and its' `npm` package dependencies and configuration options.

## UI5 Tooling

The initial building process is done by the
[UI5 command line tools](https://github.com/SAP/ui5-cli).
Run `npm run build` to build the app to `dist/`.

It is possible to "inject" custom sub-tasks into the build task of the
UI5 tooling.

Currently there are the following are defined.

# Setup

## First-Time Setup

After cloning the project, a simple `npm install` will install the modules.

## Set App Name

To change the default `test-sample-template-app-name` app name call
`npm run set-app-name` and you will be prompted for the new name.

## Set Project Namespace

To change the default `test.Sample` namespace call
`npm run set-namespace` and you will be prompted for the new namespace.

The script doesn't mind whether you enter the namespace with dots
(`ui5.hr.learning`) or slashes (`ui5/hr/learning`). Both of these
types of occurrences will be replaced (with the correct notation).

# Development - Development Time

## Naming Conventions

In order to establish a common understanding and feel for all future
projects we came up with some naming conventions we want to follow:

-   folder names should be all lower case
-   view and controller files should be in CamelCase, starting with an uppercase letter: `SomeView.controller.js`
-   function names should be CamelCase, starting with a lowercase letter: `formatTimeForDisplay(oDate)`
-   CSS selectors aka class names should be kebab-case: `my-awesome-red`
-   helper scripts should be named in kebab-case: `scripts/set-app-name`
-   a function that checks for a condition (and thus presumably returns
    a boolean value) should be prefixed `is`, like e.g. in `isLongEnough(aArray)`

## Variable Name Prefix aka Type Hinting

It is strongly recommended to use hungarian notation (name prefixes indicating
the type) for variables and object field names as indicated below.

| Sample          | Type               |
| --------------- | ------------------ |
| **s**Id         | string             |
| **o**Model      | object             |
| **\$**DomRef    | jQuery object      |
| **i**Height     | int                |
| **m**Parameters | map / assoc. array |
| **a**Entries    | array              |
| **d**Today      | date               |
| **f**Decimal    | float              |
| **b**Enabled    | boolean            |
| **r**Pattern    | RegExp             |
| **fn**Function  | function           |
| **v**Variant    | variant types      |

## Documentation

JSDoc is preferred, focus in function documentation with input and output types.

## Exact npm Package Versions

Packages already in `package.json` are pinned to exact versions.
New packages will be pinned to their latest exact version upon
installation because [`.npmrc`](.npmrc) defines `save-exact = true`.

This guarantees the consistency of the environment on different machines.

> This also means that packages will _not automatically update_ when
> running `npm` and instead _need to be updated manually_ if required.

## Development Environment

### Live Less Compiler

See also [here](#less)

During development, we compile the less files live in the browser.
There is a reference to our base `.less` file containing the rules.
These rules are then compiled by the less compiler, which during live
compiling is taken from _the current UI5 version, not_ the version defined
in `package.json`.

While there is a risk of having different versions, it is almost zero:
SAP UI5 will probably never change the less version because that would lead
to all kinds of complications, e.g. themes becoming incompatible etc.

Therefore, we assume that the version will never change and will always
be the same even in two different packages.

We inject `less.js` into `index.html` together with
[styles/style.less](./webapp/styles/style.less), which is the root
for less file inclusion. The live less.js transpiler then takes care
of loading the required files via less.js `@import`s.

### Semantic Versioning

This whole setup surrounding the commit messages opens the opportunity to
get [Semantic Versioning](https://semver.org/) for free via
[Semantic Release](https://semantic-release.gitbook.io/semantic-release/).

This is implemented in the feature branch `feature/semantic_release`.

# Deployment

An npm task `deploy` is defined in `package.json`. It uses the
[`nwabap-ui5uploader` tool](https://github.com/nrdev88/nwabap-ui5uploader)
and has its configuration living in [`.nwabaprc`](.nwabaprc).

Run it by calling `npm run deploy` after you updated the config file.

## Replace Namespace on Deploy

This [Task](./lib/tasks/replaceNamespace.js) will change all occurrences of the namespace defined in the `manifest.json` :

```json
{
  "_version": "1.7.0",
  "sap.app": {
    "id": "test.Sample", <---- This
```

with the configured `deploymentNamespace`

```yaml
specVersion: "0.1"
metadata:
    name: myproject
type: application
builder:
    customTasks:
        - name: replaceNamespace
          afterTask: generateVersionInfo
          configuration:
              deploymentNamespace: "test.Sample"
```

## Building

### Less

When building the project with `ui5 build`, `less` files will be compiled using
the [less-openui5](https://github.com/SAP/less-openui5) package as it is
defined in `package.json`.

## Remove Dev Only Block

This [task](./lib/tasks/removeDevOnlyBlocks.js) will remove blocks marked like
this in the `dist` folder from `xml` and `html` files:

```html
<!-- remove:dist -->
<link rel="stylesheet" type="text/less" href="styles/style.less" />
<!-- remove:dist -->
```

It is also possible to remove lines from the JavaScript part inside e.g. the
`index.html` like this:

```js
// <!-- remove:dist -->
sap.ui.requireSync("sap/ui/thirdparty/less");
// <!-- remove:dist -->
```

# Resources

-   [Why you should use pinned versions](http://c2fo.io/node/npm/javascript/2016/06/03/protecting-your-product-with-npm-save-exact/)
-   [Why `npx` is great](https://medium.com/@maybekatz/introducing-npx-an-npm-package-runner-55f7d4bd282b)
-   [How to add sub-tasks to the UI5 build task](https://github.com/SAP/ui5-project/blob/master/docs/BuildExtensibility.md)

# Authors

Dominik Feininger

# License

This work is [dual-licensed](../../LICENSE) under Apache 2.0 and the Derived Beer-ware License. The official license will be Apache 2.0 but finally you can choose between one of them if you use this work.

When you like this stuff, buy [@vobu](https://twitter.com/vobu) a beer or buy [@pmuessig](https://twitter.com/pmuessig) a coke when you see them.
