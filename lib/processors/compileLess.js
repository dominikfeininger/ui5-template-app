const less = require("less-openui5"),
    resourceFactory = require("@ui5/fs/lib/resourceFactory");

module.exports = function({resourcesWithNormalPath, fs}) {
    let lessBuilder = new less.Builder({fs});

    return Promise.all(
        resourcesWithNormalPath.map(resourceWithNormalPath => {
            return lessBuilder
                .build({
                    lessInputPath: resourceWithNormalPath.resource.getPath()
                })
                .then(output => {
                    return [
                        resourceFactory.createResource({
                            path: resourceWithNormalPath.normalPath.replace(/(.*).less/, "$1.css"),
                            string: output.css
                        })
                        // resourceFactory.createResource({
                        //     path: resource.getPath().replace(/(.*).less/, "$1.map"),
                        //     string: output.map
                        // })
                    ];
                });
        })
    );
};
