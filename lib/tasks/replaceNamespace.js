const replaceNamespace = require("../processors/replaceNamespace");

module.exports = function({workspace, options}) {
    return workspace
        .byGlobSource("**/manifest.json")
        .then(resources => resources[0].getString())
        .then(manifest => JSON.parse(manifest)["sap.app"].id)
        .then(nameSpaceToReplace => {
            return workspace
                .byGlob("**/*.{js,html,xml,json,less,properties}")
                .then(allResources => {
                    return replaceNamespace({
                        resources: allResources,
                        options: {
                            newNameSpace: options.configuration.deploymentNamespace,
                            nameSpaceToReplace
                        }
                    });
                })
                .then(processedResources => {
                    return Promise.all(
                        processedResources.map(resource => {
                            return workspace.write(resource);
                        })
                    );
                });
        });
};
