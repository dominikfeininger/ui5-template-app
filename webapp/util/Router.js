sap.ui.define(
    [
        "jquery.sap.global", //
        "sap/m/routing/Router"
    ],
    function(
        jQuery, //
        Router
    ) {
        "use strict";
        return Router.extend("test.Sample.util.Router", {
            /**
             * holds the id of current route
             * @type {string}
             */
            _sCurrentRoute: null,

            /**
             * Add a route to the router.
             * @override
             * @param oConfig
             * @param oParent
             */
            addRoute: function(oConfig, oParent) {
                Router.prototype.addRoute.apply(this, arguments);
                this.getRoute(oConfig.name).attachMatched(function(oEvent) {
                    this._sCurrentRoute = oEvent.getParameters().name;
                    this._oParams = oEvent.getParameters()["arguments"] || {};
                }, this);
            },

            /**
             * Get parameters for current route
             *
             * @return {object}
             */
            getCurrentRouteParams: function() {
                return this._oParams;
            },

            /**
             * will return the id of current route
             * @return {string}
             */
            getCurrentRouteId: function() {
                // this has to be the default route
                return this._sCurrentRoute ? this._sCurrentRoute : "";
            },

            /**
             * will return the current route object
             * @return {sap.ui.core.routing.Route}
             */
            getCurrentRoute: function() {
                return this.getRoute(this._sCurrentRoute);
            }
        });
    },
    /* bExport= */ true
);
