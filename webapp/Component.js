sap.ui.define(
  [
    "sap/ui/core/UIComponent",
    "sap/ui/Device",
    "./model/models",
    "./lib/willNotGetTranspiled", // only for demo purpose
    "./includes/iWillBeTranspiled", // only for demo purpose
    "sap/ui/core/ComponentSupport", // make sure to include the ComponentSupport in the bundle
    "./controller/ErrorHandler",
    "./util/Router" // load here, is missing otherwise
  ],
  (UIComponent, Device, models) => {
    "use strict";

    return UIComponent.extend("test.Sample.Component", {
      metadata: {
        manifest: "json"
      },

      /**
       * The component is initialized by UI5 automatically during the startup of the app and calls the init method once.
       * @public
       * @override
       */
      init() {
        // call the base component's init function
        UIComponent.prototype.init.apply(this, arguments);

        // enable routing
        this.getRouter().initialize();

        // set the device model
        this.setModel(models.createDeviceModel(), "device");

        //encapsulate component styling
        $("body").attr("ui-theme", "test.Sample");
      },

      /**
       * The component is destroyed by UI5 automatically.
       * In this method, the GitHandler are destroyed.
       * @public
       * @override
       */
      destroy: function() {
        // destroy all created Objects
        this._oErrorHandler.destroy();

        UIComponent.prototype.destroy.apply(this, arguments);
      },

      /**
       * This method can be called to determine whether the sapUiSizeCompact or sapUiSizeCozy
       * design mode class should be set, which influences the size appearance of some controls.
       * @public
       * @return {string} css class, either 'sapUiSizeCompact' or 'sapUiSizeCozy' - or an empty string if no css class should be set
       */
      getContentDensityClass: function() {
        if (this._sContentDensityClass === undefined) {
          // check whether FLP has already set the content density class; do nothing in this case
          if (jQuery(document.body).hasClass("sapUiSizeCozy") || jQuery(document.body).hasClass("sapUiSizeCompact")) {
            this._sContentDensityClass =
              jQuery(document.body).hasClass("sapUiSizeCozy") || jQuery(document.body).hasClass("sapUiSizeCompact");
          } else if (!Device.support.touch) {
            // apply "compact" mode if touch is not supported
            this._sContentDensityClass = "sapUiSizeCompact";
          } else {
            // "cozy" in case of touch support; default for most sap.m controls, but needed for desktop-first controls like sap.ui.table.Table
            this._sContentDensityClass = "sapUiSizeCozy";
          }
        }
        return this._sContentDensityClass;
      }
    });
  }
);
