sap.ui.define(["sap/ui/base/Object", "sap/m/MessageBox"], function(UI5Object, MessageBox) {
    "use strict";

    return UI5Object.extend("test.Sample.controller.ErrorHandler", {
        /**
         * Handles application errors by automatically attaching to the model events and displaying errors when needed.
         * @class
         * @param {sap.ui.core.UIComponent} oComponent reference to the app's component
         * @public
         * @alias test.Sample.controller.ErrorHandler
         */
        constructor: function(oComponent) {
            this._oResourceBundle = oComponent.getModel("i18n").getResourceBundle();
            this._oComponent = oComponent;
            this._oModel = oComponent.getModel();
            this._bMessageOpen = false;
            this._sErrorText = this._oResourceBundle.getText("errorText");

            this._oModel.attachMetadataFailed(function(oEvent) {
                let oParams = oEvent.getParameters();
                this._showMetadataError(oParams.response);
            }, this);

            this._oModel.attachRequestFailed(function(oEvent) {
                let oParams = oEvent.getParameters();

                let sMessage = this._parseResponseMessage(oParams.response);
                this._showServiceError(sMessage);
            }, this);
        },

        /* =========================================================== */
        /* begin: internal methods                                     */
        /* =========================================================== */

        /**
         * Shows a {@link sap.m.MessageBox} when the metadata call has failed.
         * The user can try to refresh the metadata.
         * @param {string} sDetails a technical error to be displayed on request
         * @private
         * @memberof test.Sample.controller.ErrorHandler.prototype
         */
        _showMetadataError: function(vDetails) {
            let sDetails;
            //parse XML body if returned value was a XML string
            if (typeof vDetails === "object" && vDetails.body) {
                let oXMLDocument = jQuery.sap.parseXML(vDetails.body);
                let aMessages = oXMLDocument.getElementsByTagName("message");
                if (aMessages.length > 0) {
                    sDetails = aMessages[0].textContent;
                }
            } else {
                sDetails = vDetails;
            }

            setTimeout(
                function() {
                    MessageBox.error(this._sErrorText, {
                        id: "metadataErrorMessageBox",
                        details: sDetails,
                        styleClass: this._oComponent.getContentDensityClass(),
                        actions: [MessageBox.Action.RETRY, MessageBox.Action.CLOSE],
                        onClose: function(sAction) {
                            if (sAction === MessageBox.Action.RETRY) {
                                this._oModel.refreshMetadata();
                            }
                        }.bind(this)
                    });
                }.bind(this)
            );
        },

        /**
         * Shows a {@link sap.m.MessageBox} when a service call has failed.
         * Only the first error message will be display.
         * @param {string} sDetails a technical error to be displayed on request
         * @private
         * @memberof test.Sample.controller.ErrorHandler.prototype
         */
        _showServiceError: function(oError) {
            if (this._bMessageOpen) {
                return;
            }
            this._bMessageOpen = true;
            MessageBox.error(oError.message.value, {
                id: "serviceErrorMessageBox",
                details: `<pre>${JSON.stringify(oError.innererror.errordetails, null, 4)}</pre>`,
                styleClass: this._oComponent.getContentDensityClass(),
                actions: [MessageBox.Action.CLOSE],
                onClose: function() {
                    this._bMessageOpen = false;
                }.bind(this)
            });
        },

        /**
         * Parses a request response and extracts its error message (if any is present). This works with JSON and XML strings given in response
         * @param oResponseInfo
         * @return {string}
         * @private
         */
        _parseResponseMessage: function(oResponseInfo) {
            if (!oResponseInfo) {
                return {};
            }

            var sMessage = "";
            var sContentFormat = "json";
            var sResponse = oResponseInfo.responseText || "";

            if (oResponseInfo.headers && oResponseInfo.headers["Content-Type"]) {
                var sContentType = oResponseInfo.headers["Content-Type"];

                if (sContentType.indexOf("xml") !== -1) {
                    sContentFormat = "xml";
                }
            }

            if (sContentFormat === "xml") {
                var oXMLDocument = jQuery.sap.parseXML(sResponse);

                sMessage = jQuery(oXMLDocument)
                    .find("error > message")
                    .text();
            } else if (sContentFormat === "json") {
                var oResponse = JSON.parse(sResponse);

                if (oResponse && oResponse.error && oResponse.error.message && oResponse.error.message.value) {
                    sMessage = oResponse.error.message.value;
                }
            }

            return sMessage;
        }
    });
});
