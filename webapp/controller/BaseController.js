sap.ui.define(
    ["sap/ui/core/mvc/Controller", "sap/ui/core/routing/History"],
    /**
     * @param {typeof sap.ui.core.mvc.Controller} Controller
     * @param {typeof sap.ui.core.routing.History} History
     * @yields {typeof sap.ui.core.mvc.Controller}
     */
    (Controller, History) => {
        "use strict";

        return Controller.extend("test.Sample.controller.BaseController", {
            /**
             * inits on controller instantiation
             */
            onInit() {},

            /**
             * Convenience method for accessing the router in every controller of the application.
             * @public
             * @returns {sap.ui.core.routing.Router} the router for this component
             */
            getRouter() {
                return this.getOwnerComponent().getRouter();
            },

            /**
             * Convenience method for getting the view model by name in every controller of the application.
             * @public
             * @param {string} sName the model name
             * @returns {sap.ui.model.Model} the model instance
             */
            getModel(sName) {
                return this.getView().getModel(sName);
            },

            /**
             * Set a (named) model on this controller's view.
             *
             * @param {sap.ui.model.Model} oModel the model instance
             * @param {string} [sName] the model name
             * @returns {sap.ui.base.ManagedObject} _this_ to allow method chaining
             */
            setModel: function(oModel, sName) {
                this.getView().setModel(oModel, sName);
                // setModel() would return the view, but since we are in the
                // controller, we'd rather have 'this' returned instead.
                return this;
            },

            /**
             * Convenience method for getting the resource bundle.
             * @public
             * @returns {sap.ui.model.resource.ResourceModel} the resourceModel of the component
             */
            geti18nResourceBundle() {
                return this.getOwnerComponent()
                    .getModel("i18n")
                    .getResourceBundle();
            },

            /**
             * Event handler for navigating back.
             * It there is a history entry we go one step back in the browser history
             * If not, it will replace the current entry of the browser history with the master route.
             * @public
             */
            onNavBack() {
                const sPreviousHash = History.getInstance().getPreviousHash();

                if (sPreviousHash !== undefined) {
                    // eslint-disable-next-line
                    history.go(-1);
                } else {
                    this.getRouter().navTo("RouteMain", {}, true);
                }
            },

            onAfterRendering: function() {}
        });
    }
);
